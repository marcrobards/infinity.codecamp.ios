//
//  Tweet.m
//  CodeCamp.iOS
//
//  Created by Marc Robards on 5/8/12.
//  Copyright (c) 2012 Infinity Software Inc.. All rights reserved.
//

#import "Tweet.h"
#import "user.h"

@implementation Tweet

@synthesize tweetID = _tweetID,
            text = _text,
            dateCreated = _dateCreated,
            user = _user;

@end
