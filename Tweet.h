//
//  Tweet.h
//  CodeCamp.iOS
//
//  Created by Marc Robards on 5/8/12.
//  Copyright (c) 2012 Infinity Software Inc.. All rights reserved.
//

#import <Foundation/Foundation.h>
@class User;

@interface Tweet : NSObject

@property (nonatomic, retain) NSNumber *tweetID;
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) NSDate *dateCreated;

@property (nonatomic, retain) User *user;

@end
