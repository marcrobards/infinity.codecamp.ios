//
//  User.h
//  CodeCamp.iOS
//
//  Created by Marc Robards on 5/7/12.
//  Copyright (c) 2012 Infinity Software Inc.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Twitter/Twitter.h>

@interface User: NSObject <NSCoding>

@property (nonatomic, retain) NSString *idStr;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *screenName;
@property (nonatomic, retain) NSString *profileImageURL;
@property (nonatomic, retain) UIImage *profileImage;
@property (nonatomic, retain) NSArray *tweets;

+ (UIImage *)getUserImage:(User *)user;

@end
