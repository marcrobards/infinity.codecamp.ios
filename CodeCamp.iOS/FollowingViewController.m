//
//  FollowingViewController.m
//  CodeCamp.iOS
//
//  Created by Marc Robards on 5/7/12.
//  Copyright (c) 2012 Infinity Software Inc.. All rights reserved.
//

#import "FollowingViewController.h"
#import "TweetsViewController.h"
#import "User.h"

@interface FollowingViewController ()

@property (strong, nonatomic) NSMutableData *webData;
@property (strong, nonatomic) NSArray *userData;


@end

@implementation FollowingViewController

@synthesize followingUsers = _followingUsers,
            webData = _webData,
            userData = _userData;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
            }
    return self;
}

- (void)getUserData:(User *)user
{   
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    //  Now we can create our request.  Note that we are performing a GET request.
    TWRequest *request = [[TWRequest alloc] initWithURL:
                                            [NSURL URLWithString:[NSString stringWithFormat:@"https://api.twitter.com/1/users/lookup.json?screen_name=%@&include_entities=true", user.screenName]] 
                                             parameters:nil
	                                      requestMethod:TWRequestMethodGET];
    //  Perform our request
    [request performRequestWithHandler:
     ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
         
         if (responseData) {
             
             if ([urlResponse statusCode] == 200)
             {
                              
                 //  Use the NSJSONSerialization class to parse the returned JSON
                 NSError *jsonError;
                 NSArray *timeline = 
                 [NSJSONSerialization JSONObjectWithData:responseData 
                                                 options:NSJSONReadingMutableLeaves 
                                                   error:&jsonError];
                 
                 if (timeline) {
                     // We have an object that we can parse
                     NSLog(@"%@", timeline);                                  
                     
                     NSDictionary *userDict = [timeline objectAtIndex:0];
                     if ([userDict count] > 0)
                     {
                         user.idStr = [userDict objectForKey:@"id_str"];
                         user.name = [userDict objectForKey:@"name"];
                         user.profileImageURL = [userDict objectForKey:@"profile_image_url"];                    
                         
                         [self saveData];                 
                     }      
                     dispatch_sync(dispatch_get_main_queue(), ^{
                         [self.tableView reloadData];
                         [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                     });
                 }        
              
                 else { 
                     // Inspect the contents of jsonError
                     NSLog(@"%@", jsonError);
                 }
             }
             else 
             {
                 dispatch_sync(dispatch_get_main_queue(), ^{
                     UIAlertView *message = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"User %@ Not Found.", user.screenName] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                     [message setAlertViewStyle:UIAlertViewStyleDefault];
                     
                     [message show];
                     
                     [self.followingUsers removeObject:user];
                     [self.tableView reloadData];
                     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                 });
             }
         }
     }];
}


-(void)loadData
{
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *savePath = [rootPath stringByAppendingPathComponent:@"followingData"]; 
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:savePath])
    {
        NSData *data = [fileManager contentsAtPath:savePath];
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data]; 
        self.followingUsers = [unarchiver decodeObjectForKey:@"DataArray"];        
        [self.tableView reloadData];
    } 
}

- (void)saveData
{
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *savePath = [rootPath stringByAppendingPathComponent:@"followingData"]; 
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableData *saveData = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:saveData];
    
    [archiver encodeObject:self.followingUsers forKey:@"DataArray"]; 
    [archiver finishEncoding];
    [fileManager createFileAtPath:savePath contents:saveData attributes:nil]; 
}


- (void)viewDidLoad
{
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    self.followingUsers = [[NSMutableArray alloc] init];    
    
    [self loadData];
    [super viewDidLoad];

}

- (void)viewDidUnload
{
     [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.followingUsers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FollowingCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    User *user = [self.followingUsers objectAtIndex:indexPath.row];
    
    if ([user.name length] == 0)
    {
        cell.detailTextLabel.text = nil;
        cell.textLabel.text = [NSString stringWithFormat:@"Loading @%@...",user.screenName];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.imageView.image = nil;   
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }
    else 
    {
        cell.textLabel.text = user.name;   
        cell.detailTextLabel.text = [NSString stringWithFormat:@"@%@", user.screenName];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.imageView.image = [User getUserImage:user];    
    }
    return cell;
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    [self.tableView setEditing:editing animated:animated]; 
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    [self.followingUsers exchangeObjectAtIndex:fromIndexPath.row withObjectAtIndex:toIndexPath.row];
    [self saveData];
    [self.tableView reloadData];    
}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier hasPrefix:@"Shows Users Tweets"]) 
    {
        TweetsViewController *tweetsViewController = [segue destinationViewController];
        tweetsViewController.user = sender;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    User *user = [self.followingUsers objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"Shows Users Tweets" sender:user];   
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCellEditingStyle result = UITableViewCellEditingStyleNone;
    if ([tableView isEqual:self.tableView])
    { 
        result = UITableViewCellEditingStyleDelete;
    }
    return result; 
}


- (void) tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    { 
        if (indexPath.row < [self.followingUsers count])
        {
            /* First remove this object from the source */ 
            [self.followingUsers removeObjectAtIndex:indexPath.row];
            /* Then remove the associated cell from the Table View */
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self saveData];            
        }
    }
}
         
- (IBAction)AddFollower:(id)sender 
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Enter Twitter Screen Name:"
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Follow", nil];
    
    [message setAlertViewStyle:UIAlertViewStylePlainTextInput];
    
    [message show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Follow"])
    {
        UITextField *username = [alertView textFieldAtIndex:0];        
        NSLog(@"Username: %@", username.text);        
        
        User *user = [[User alloc] init];
        user.screenName = username.text;       
        [self.followingUsers addObject:user];
        [self getUserData:user];
        [self.tableView reloadData];
    }
}

@end
