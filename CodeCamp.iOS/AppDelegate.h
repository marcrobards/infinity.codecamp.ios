//
//  AppDelegate.h
//  CodeCamp.iOS
//
//  Created by Marc Robards on 5/7/12.
//  Copyright (c) 2012 Infinity Software Inc.. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
