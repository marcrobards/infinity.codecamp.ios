//
//  TweetsViewController.m
//  CodeCamp.iOS
//
//  Created by Marc Robards on 5/7/12.
//  Copyright (c) 2012 Infinity Software Inc.. All rights reserved.
//

#import "TweetsViewController.h"
#import "User.h"
#import "Tweet.h"
#import "TweetCell.h"

@interface TweetsViewController ()

@property (nonatomic, retain) NSMutableArray *tweetList;

@end

@implementation TweetsViewController

@synthesize user = _user,
            userList = _userList,
            tweetList = _tweetList;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

// Get the time different between the announcement time and now
- (NSString *)getTimeSinceAnnouncement:(NSDate *)startDate
{
    NSDate *endDate = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    
    // Set which calendar data to return
    NSUInteger unitFlags = NSYearCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
    
    // Get the different time time
    NSDateComponents *components = [gregorian components:unitFlags
                                                fromDate:startDate
                                                  toDate:endDate options:0];
    
    // Get the difference values
    NSInteger minutes = [components minute];
    NSInteger hours = [components hour];
    NSInteger days = [components day];
    NSInteger weeks = [components week];
    NSInteger years = [components year];
    
    // Start at the biggest time frame and move downward. The bigger time frames will be zero for smaller time frames
    if(years > 0)
    {
        return [NSString stringWithFormat:@"%dy ago", years];
    }
    
    if(weeks >= 1 && weeks <= 52)
    {
        return [NSString stringWithFormat:@"%dw ago", weeks];
    }
    
    if(days >= 1 && days <= 7)
    {
        return [NSString stringWithFormat:@"%dd ago", days];
    }
    
    if(hours >= 1 && hours <= 23)
    {
        return [NSString stringWithFormat:@"%dh ago", hours];
    }
    
    // Default smallest time frame
    return [NSString stringWithFormat:@"%dm ago", minutes];
}

-(void)loadData
{
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *savePath = [rootPath stringByAppendingPathComponent:@"followingData"]; 
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:savePath])
    {
        NSData *data = [fileManager contentsAtPath:savePath];
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data]; 
        self.userList = [unarchiver decodeObjectForKey:@"DataArray"];        
    } 
}

- (void)loadTweets:(User *)user
{    
    user.profileImage = [User getUserImage:user];
    self.tweetList = [[NSMutableArray alloc] init];    
    //  Now we can create our request.  Note that we are performing a GET request.
    TWRequest *request = [[TWRequest alloc] initWithURL:
                          [NSURL URLWithString:[NSString stringWithFormat:@"https://api.twitter.com/1/statuses/user_timeline.json?screen_name=%@", user.screenName]] 
                                             parameters:nil
	                                      requestMethod:TWRequestMethodGET];
    //  Perform our request
    [request performRequestWithHandler:
     ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
         
         if (responseData) {
             //  Use the NSJSONSerialization class to parse the returned JSON
             NSError *jsonError;
             NSArray *timeline = 
             [NSJSONSerialization JSONObjectWithData:responseData 
                                             options:NSJSONReadingMutableLeaves 
                                               error:&jsonError];
             
             if (timeline) {
                 // We have an object that we can parse
                 NSLog(@"%@", timeline);
                 for (NSDictionary *tw in timeline) 
                 {
                     Tweet *tweet = [[Tweet alloc]init];
                     tweet.tweetID = [tw objectForKey:@"id_str"];
                     tweet.text = [tw objectForKey:@"text"];
                     // Mon Jun 27 19:32:19 +0000 2011
                     NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                     [dateFormat setDateFormat:@"eee MMM dd HH:mm:ss ZZZ yyyy"];
                     tweet.dateCreated = [dateFormat dateFromString:[tw objectForKey:@"created_at"]];                      
                     tweet.user = user;
                     [self.tweetList addObject:tweet];                     
                 }   
                 dispatch_sync(dispatch_get_main_queue(), ^{
                     NSSortDescriptor *sortDescriptor;
                     sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateCreated"
                                                                  ascending:NO];
                     NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                     NSArray *sortedArray;
                     sortedArray = [self.tweetList sortedArrayUsingDescriptors:sortDescriptors];
                     self.tweetList = [NSMutableArray arrayWithArray:sortedArray];
                     [self.tableView reloadData];
                 });
             } 
             else { 
                 // Inspect the contents of jsonError
                 NSLog(@"%@", jsonError);
             }
         }
     }];
}

- (void)loadAllTweets
{    
    if (self.user.name)
    {
        [self setTitle:self.user.name];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        [self loadTweets:self.user];
    }
    else 
    {
        [self setTitle:@"All Tweets"];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        [self loadData];
        for (User *user in self.userList) 
        {
            [self loadTweets:user];
        }
    }
}

- (void)viewDidLoad
{
    [self loadAllTweets];
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return [self.tweetList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    static NSString *CellIdentifier = @"Tweet Cell";
    TweetCell *cell = (TweetCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if ([self.tweetList count] == 0)
    {
        cell.text.text = @"Loading Tweets...";
    }
    else 
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        Tweet *tweet = [self.tweetList objectAtIndex:indexPath.row];
        
        cell.name.text = tweet.user.name;
        cell.screenName.text = [NSString stringWithFormat:@"@%@", tweet.user.screenName];
        cell.profileImage.image = tweet.user.profileImage;
        cell.text.text = tweet.text;    
        cell.timeSincePost.text = [self getTimeSinceAnnouncement:tweet.dateCreated];
    }
    return cell;
}


#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 87;
}

- (IBAction)RefreshTweets:(id)sender 
{
    [self loadAllTweets];
}
@end
