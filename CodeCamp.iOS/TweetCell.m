//
//  TweetCell.m
//  CodeCamp.iOS
//
//  Created by Marc Robards on 5/8/12.
//  Copyright (c) 2012 Infinity Software Inc.. All rights reserved.
//

#import "TweetCell.h"

@implementation TweetCell

@synthesize name = _name,
            screenName = _screenName,
            timeSincePost = _timeSincePost,
            text = _text,       
            profileImage = _profileImage;
            

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
