//
//  FollowingViewController.h
//  CodeCamp.iOS
//
//  Created by Marc Robards on 5/7/12.
//  Copyright (c) 2012 Infinity Software Inc.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Twitter/Twitter.h>

@interface FollowingViewController : UITableViewController
{
    NSURL *url;
    NSURLConnection *urlConnection;
    int statusCode;
}

@property (nonatomic, retain) NSMutableArray *followingUsers;

- (IBAction)AddFollower:(id)sender;

@end
