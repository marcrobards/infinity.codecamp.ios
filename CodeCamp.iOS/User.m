//
//  User.m
//  CodeCamp.iOS
//
//  Created by Marc Robards on 5/7/12.
//  Copyright (c) 2012 Infinity Software Inc.. All rights reserved.
//

#import "User.h"

@implementation User
@synthesize idStr = _idStr,
            name = _name,
            screenName = _screenName,
            profileImageURL = _profileImageURL,
            profileImage = _profileImage,
            tweets = _tweets;

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self)
    {
        self.idStr = [decoder decodeObjectForKey:@"idStr"]; 
        self.name = [decoder decodeObjectForKey:@"name"]; 
        self.screenName = [decoder decodeObjectForKey:@"screenName"]; 
        self.profileImageURL = [decoder decodeObjectForKey:@"profileImageURL"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder 
{
    [encoder encodeObject:self.idStr forKey:@"idStr"]; 
    [encoder encodeObject:self.name forKey:@"name"]; 
    [encoder encodeObject:self.screenName forKey:@"screenName"]; 
    [encoder encodeObject:self.profileImageURL forKey:@"profileImageURL"];
}

+ (UIImage *)getUserImage:(User *)user
{
    // get image path
    NSString *imagePath = user.profileImageURL;    
    
    // get image name
    NSRange range = [imagePath rangeOfString:@"/" options:NSBackwardsSearch];
    NSString *imageName = [imagePath substringFromIndex:range.location + 1];    
    
    // get the application document directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;    
    
    // check if file exists locally    
    NSString *localImagePath = [basePath stringByAppendingPathComponent:imageName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:localImagePath];
    
    // if exists, use local file
    if (fileExists) 
    {
        UIImage *profileImage = [UIImage imageWithData: [NSData dataWithContentsOfFile:localImagePath]];        
        return profileImage;
    }
    else 
    {   
        UIImage *img = [[UIImage alloc] initWithData:
                        [NSData dataWithContentsOfURL:
                         [NSURL URLWithString:user.profileImageURL]]];       
        return img;
    }   

}

@end
