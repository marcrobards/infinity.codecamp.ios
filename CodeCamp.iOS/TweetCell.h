//
//  TweetCell.h
//  CodeCamp.iOS
//
//  Created by Marc Robards on 5/8/12.
//  Copyright (c) 2012 Infinity Software Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TweetCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *screenName;
@property (weak, nonatomic) IBOutlet UILabel *timeSincePost;
@property (weak, nonatomic) IBOutlet UITextView *text;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@end
