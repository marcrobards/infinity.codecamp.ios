//
//  TweetsViewController.h
//  CodeCamp.iOS
//
//  Created by Marc Robards on 5/7/12.
//  Copyright (c) 2012 Infinity Software Inc.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Twitter/Twitter.h>

@class User;

@interface TweetsViewController : UITableViewController

@property (nonatomic, retain) User *user;
@property (nonatomic, retain) NSArray *userList;

- (IBAction)RefreshTweets:(id)sender;

@end
